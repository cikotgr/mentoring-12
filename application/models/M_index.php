<?php

class M_index extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_jumlah_alsintan($id_jenis) {

        $sql = "SELECT COUNT(id_alsintan) as jumlah from alsintan WHERE id_jenis_alsintan=?";
        $query = $this->db->query($sql, array($id_jenis))->row_array();
        $jumlah = (int) $query['jumlah'];
        return $jumlah;
    }

    function get_kabupaten() {
        $query = $this->db->query("SELECT id_kabupaten, nama_kabupaten  FROM kabupaten WHERE is_active='1'")->result_array();
        return $query;
    }

    function get_kecamatan() {
        $where = '';
        $id_kabupaten = $this->input->get('kabupaten');

        if (!empty($id_kabupaten)) {
            if ($id_kabupaten == 'all') {
                $where = '';
            } else {
                $where .= " AND id_kabupaten ='$id_kabupaten'";
            }
        }
        $query = $this->db->query("SELECT id_kecamatan, nama_kecamatan  FROM kecamatan WHERE is_active='1' $where")->result_array();
        return $query;
    }

    function get_data() {
        $sql = "SELECT id_kabupaten, kode_kabupaten, nama_kabupaten FROM kabupaten WHERE is_active='1'";
        $query=$this->db->query($sql);
        return $query->result_array();
    }

    function get_rekapitulasi($jenis_data, $id_kabupaten) {
        // Jenis data 0: Total Data, 1. Jumlah Sudah dimanfaatakn 2. Belum dimanfaatkan, 3. tidak layak
        $and_sql = "";
        if ($jenis_data == 0) {
            $and_sql = " ";
        } else if ($jenis_data == "1") {
            $and_sql = " AND status='1'";
        } else if ($jenis_data == "2") {
            $and_sql = " AND status='0'";
        } else if ($jenis_data == "3") {
            $and_sql = " AND kondisi='0'";
        }
        $id_jenis_alsintan = $this->input->get('jenis');
        if (!empty($id_jenis_alsintan)) {
            if ($id_jenis_alsintan == 'all') {
                
            } else {
                $and_sql .= " AND id_jenis_alsintan = '$id_jenis_alsintan'";
            }
        }

        $query = "SELECT COUNT(id_alsintan) as jumlah FROM alsintan WHERE id_kabupaten=? $and_sql";

        $query = $this->db->query($query, array($id_kabupaten))->row_array();
        $jumlah = (int) $query['jumlah'];
        return $jumlah;
    }

}
