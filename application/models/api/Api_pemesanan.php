<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_pemesanan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_detail($id_pemesanan)
    {
        $pesanan = $this->db->query("SELECT id_pemesanan,nama,nik, nama_program,email, nama_grup,tanggal_keberangkatan, total_pembayaran, durasi_hari from um_pemesanan left join customer on REPLACE(customer.id_customer,'-','')=REPLACE(um_pemesanan.id_customer,'-','') left join um_kelas_program on REPLACE(um_kelas_program.id_kelas_program,'-','')=REPLACE(um_pemesanan.fk_program_umrah,'-','') left join um_grup_keberangkatan on REPLACE(um_grup_keberangkatan.id_grup,'-','')=REPLACE(um_kelas_program.fk_grup_keberangkatan,'-','') where REPLACE(id_pemesanan,'-','')=?",array(str_replace("-", "", $id_pemesanan)));
        if ($pesanan->num_rows()!=0) {

            $k = $pesanan->row();

            $result['id_pemesanan'] = $k->id_pemesanan;
            $result['nama'] = $k->nama;
            $result['email'] = $k->email;
            $result['nik'] = $k->nik;
            $result['nama_program'] = $k->nama_program;
            $result['nama_grup'] = $k->nama_grup;
            $result['tanggal_keberangkatan'] = $k->tanggal_keberangkatan;
            $result['durasi_hari'] = $k->durasi_hari;
            $result['total_pembayaran'] = $k->total_pembayaran;

            $jamaah = $this->db->query("SELECT nama_lengkap, kategori, pilihan_kamar,harga  from um_pemesanan_jamaah where fk_id_pemesanan=?", array($result['id_pemesanan']));
            $dt_jamaah = array();
            $j=0;
            foreach ($jamaah->result() as $key => $r) {
                $dt_jamaah[$j]['nama_lengkap'] = $r->nama_lengkap;
                $dt_jamaah[$j]['kategori'] = $r->kategori;
                $dt_jamaah[$j]['harga'] = $r->harga;
                $dt_jamaah[$j]['pilihan_kamar'] = $r->pilihan_kamar;
                $j++;
            }

            $result['jamaah'] = $dt_jamaah;

            return ['status'=>'ok','message'=>'data ditemukan.','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>'0'];
        }
    }

    public function pemesanan_customer($id_customer)
    {
        $pesanan = $this->db->query("SELECT id_pemesanan,tanggal_jatuh_tempo_dp,status_pembayaran,nama,nik, nama_program,email, nama_grup,tanggal_keberangkatan, total_pembayaran, durasi_hari from um_pemesanan left join customer on REPLACE(customer.id_customer,'-','')=REPLACE(um_pemesanan.id_customer,'-','') left join um_kelas_program on REPLACE(um_kelas_program.id_kelas_program,'-','')=REPLACE(um_pemesanan.fk_program_umrah,'-','') left join um_grup_keberangkatan on REPLACE(um_grup_keberangkatan.id_grup,'-','')=REPLACE(um_kelas_program.fk_grup_keberangkatan,'-','') where REPLACE(customer.id_customer,'-','')=?",array(str_replace("-", "", $id_customer)));
        if ($pesanan->num_rows()!=0) {
            $i=0;
            foreach ($pesanan->result() as $k) {

                $result[$i]['id_pemesanan'] = $k->id_pemesanan;
                $result[$i]['tanggal_jatuh_tempo_dp'] = $k->tanggal_jatuh_tempo_dp;
                $result[$i]['status_pembayaran'] = $k->status_pembayaran;
                $result[$i]['nama'] = $k->nama;
                $result[$i]['email'] = $k->email;
                $result[$i]['nik'] = $k->nik;
                $result[$i]['nama_program'] = $k->nama_program;
                $result[$i]['nama_grup'] = $k->nama_grup;
                $result[$i]['tanggal_keberangkatan'] = $k->tanggal_keberangkatan;
                $result[$i]['durasi_hari'] = $k->durasi_hari;
                $result[$i]['total_pembayaran'] = $k->total_pembayaran;

                $jamaah = $this->db->query("SELECT nama_lengkap, kategori, pilihan_kamar,harga  from um_pemesanan_jamaah where fk_id_pemesanan=?", array($result[$i]['id_pemesanan']));
                $dt_jamaah = array();
                $j=0;
                foreach ($jamaah->result() as $key => $r) {
                    $dt_jamaah[$j]['nama_lengkap'] = $r->nama_lengkap;
                    $dt_jamaah[$j]['kategori'] = $r->kategori;
                    $dt_jamaah[$j]['harga'] = $r->harga;
                    $dt_jamaah[$j]['pilihan_kamar'] = $r->pilihan_kamar;
                    $j++;
                }
                $result[$i]['jamaah'] = $dt_jamaah;
                $i++;
            }
            return ['status'=>'ok','message'=>'data ditemukan.','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>'0'];
        }
    }

    public function detail_pesanan($id)
    {
        $query = $this->db->query("SELECT upm.id_pemesanan, upm.prefix,upm.status_pembayaran,upm.metode_pembayaran, upm.total_pembayaran,gk.tanggal_keberangkatan,
    gk.nama_grup, gk.durasi_hari, cs.kode_customer, cs.email, cs.nik, cs.nama,cs.nomor_hp, cs.tempat_lahir, cs.tanggal_lahir,cs.jenis_kelamin
    FROM um_pemesanan upm
    left join um_kelas_program kp on REPLACE(kp.id_kelas_program,'-','')=REPLACE(upm.fk_program_umrah,'-','')
    left join um_grup_keberangkatan gk on REPLACE(gk.id_grup,'-','')=REPLACE(kp.fk_grup_keberangkatan,'-','')
    left join customer cs on REPLACE(cs.id_customer,'-','') = REPLACE(upm.id_customer,'-','')
    where REPLACE(upm.id_pemesanan,'-','')=?", array(str_replace("-", "", $id)));
        if ($query->num_rows()!=0) {
            $k = $query->row();

            $result['id_pemesanan'] = $k->id_pemesanan;
            $result['prefix'] = $k->prefix;
            $result['status_pembayaran'] = $k->status_pembayaran;
            $result['metode_pembayaran'] = $k->metode_pembayaran;
            $result['total_pembayaran'] = $k->total_pembayaran;
            $result['tanggal_keberangkatan'] = $k->tanggal_keberangkatan;
            $result['nama_grup'] = $k->nama_grup;
            $result['durasi_hari'] = $k->durasi_hari;
            $result['kode_customer'] = $k->kode_customer;
            $result['email'] = $k->email;
            $result['nik'] = $k->nik;
            $result['nama'] = $k->nama;
            $result['nomor_hp'] = $k->nomor_hp;
            $result['tanggal_lahir'] = $k->tanggal_lahir;
            $result['jenis_kelamin'] = $k->jenis_kelamin;

            $jamaah = $this->db->query("SELECT id_jamaah,nama_lengkap, jenis_identitas, nomor_identitas,jenis_kelamin, kategori, tempat_lahir, tanggal_lahir, pendidikan, pekerjaan,alamat_ktp, email, nomor_hp, pilihan_kamar, harga from um_pemesanan_jamaah where REPLACE(fk_id_pemesanan,'-','')=?", array(str_replace("-", "", $result['id_pemesanan'])));
            $result['jamaah'] = $jamaah->result();

            return ['status'=>'ok','message'=>'data ditemukan','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data tidak ditemukan','data'=>'0'];
        }
    }

}
