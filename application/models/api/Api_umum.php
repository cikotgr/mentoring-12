<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_umum extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function data_slider() {

        $get = $this->db->query("SELECT * FROM slider where is_active=1 order by created_at ASC");
        if ($get->num_rows()>0) {

            $i=0;
            foreach ($get->result() as $rows) {
                $slider[$i]['id_slider'] = $rows->id_slider;
                $slider[$i]['judul_slider'] = $rows->judul_slider;
                $slider[$i]['caption_slider'] = $rows->caption_slider;
                $slider[$i]['foto_slider'] = base_url($rows->foto_slider);
                $i++;
            }

            return [
                'status'=>'ok',
                'message'=>'data slider ditemukan',
                'data'=>$slider];

        } else {
            return [
                'status'=>'failed',
                'message'=>'Data slider kosong',
                'data'=>''];
        }
    }

}
