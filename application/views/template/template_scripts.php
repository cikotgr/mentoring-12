<!-- jQuery -->
<script src="<?=base_url('')?>public/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?=base_url('')?>public/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge("uibutton", $.ui.button);
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?=base_url('public/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
    <!-- ChartJS -->
    <script src="<?=base_url('public/plugins/chart.js/Chart.min.js')?>"></script>
    <!-- Sparkline -->
    <script src="<?=base_url('public/plugins/sparklines/sparkline.js')?>"></script>
    <!-- JQVMap -->
    <script src="<?=base_url('public/plugins/jqvmap/jquery.vmap.min.js')?>"></script>
    <script src="<?=base_url('public/plugins/jqvmap/maps/jquery.vmap.usa.js')?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=base_url('public/plugins/jquery-knob/jquery.knob.min.js')?>"></script>
    <!-- daterangepicker -->
    <script src="<?=base_url('public/plugins/moment/moment.min.js')?>"></script>
    <script src="<?=base_url('public/plugins/daterangepicker/daterangepicker.js')?>"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="<?=base_url('public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')?>"></script>
    <!-- Summernote -->
    <script src="<?=base_url('public/plugins/summernote/summernote-bs4.min.js')?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?=base_url('public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url('public/dist/js/adminlte.js')?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url('public/dist/js/demo.js')?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?=base_url('public/dist/js/pages/dashboard.js')?>"></script>

    
<!-- Page Specific JS File -->
<script type="text/javascript">
  $(function() {
    $('.uang').mask('000.000.000.000.000', {
      reverse: true
    });

    $('.tanggal').datepicker({
      format: 'yyyy-mm-dd',
      language: 'id',
      autoclose: true
    });
  })

  function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

      return false;
    return true;
  }

  function show_loading() {
    document.getElementById("spinner-front").classList.add("show");
    document.getElementById("spinner-back").classList.add("show");
  }

  function hide_loading() {
    document.getElementById("spinner-front").classList.remove("show");
    document.getElementById("spinner-back").classList.remove("show");
  }

  function readURL(input, type) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        if (type == "simpan") {
          $('#tampil_sim').attr('src', e.target.result);

        } else if (type == "profil") {
          $('#tampil_sim_profil').attr('src', e.target.result);

        }else if (type == "edit") {
          $('#tampil_up').attr('src', e.target.result);

        } else if (type = "scan1") {
          $('#tampil_scan1').attr('src', e.target.result);

        } else if (type = "scan2") {
          $('#tampil_scan2').attr('src', e.target.result);

        }
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
</script>