<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?php echo $this->config->item('webname') ?></title>

    <!-- Google Font: Source Sans Pro -->
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"
    />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/fontawesome-free/css/all.min.css') ?>" />
    <!-- Ionicons -->
    <link
      rel="stylesheet"
      href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
    />
    <!-- Tempusdominus Bootstrap 4 -->
    <link
      rel="stylesheet"
      href="<?= base_url('public/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') ?>"
    />
    <!-- iCheck -->
    <link
      rel="stylesheet"
      href="<?= base_url('public/plugins/icheck-bootstrap/icheck-bootstrap.min.css') ?>"
    />
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/jqvmap/jqvmap.min.css') ?>" />
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('public/dist/css/adminlte.min.css') ?>" />
    <!-- overlayScrollbars -->
    <link
      rel="stylesheet"
      href="<?= base_url('public/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') ?>"
    />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/daterangepicker/daterangepicker.css') ?>" />
    <!-- summernote -->
    <link rel="stylesheet" href="<?= base_url('public/plugins/summernote/summernote-bs4.min.css') ?>" />
  </head>
  <?php
if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
        echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
}
?>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div id="app">

    <div class="wrapper">
    
      <!-- Preloader -->
      <div
        class="preloader flex-column justify-content-center align-items-center"
      >
        <img
          class="animation__shake"
          src="<?= base_url('public/dist/img/AdminLTELogo.png')?>"
          alt="AdminLTELogo"
          height="60"
          width="60"
        />
      </div>

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"
              ><i class="fas fa-bars"></i
            ></a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
          
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="fas fa-th-large"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="<?php echo site_url('/index/logout'); ?>" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
        <img src="https://adminlte.io/themes/v3/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light"><?php echo $this->config->item('webname') ?></span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex ">
            <div class="info ">
              <a href="#" class="d-block"><?php echo $this->session->userdata('admin_nama'); ?></a>
            </div>
          </div>
          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul
              class="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
            <li class="<?php echo is_active_page('index', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('index'); ?>"><i class="fa fa-home"></i>
                <span>Beranda</span></a>
            </li>
              <?php if (in_array("akun.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('akun', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('akun/index') ?>"><i class="fas fa-th-list"> </i><span> Akun</span></a>
                </li>
              <?php endif; ?>
              <?php if (in_array("kategori.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('kategori', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('kategori/index') ?>"><i class="fas fa-tags"> </i><span> Kategori</span></a>
                </li>
              <?php endif; ?>
              <?php if (in_array("produk.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('produk', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('produk/index') ?>"><i class="fas fa-tags"> </i><span> Produk</span></a>
                </li>
              <?php endif; ?>
              <?php if (in_array("lembaga.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('lembaga', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('lembaga/index') ?>"><i class="fas fa-image"> </i><span> Lembaga</span></a>
                </li>
              <?php endif; ?>
              <?php if (in_array("devisi.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('devisi', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('devisi/index') ?>"><i class="fas fa-tags"> </i><span> Devisi</span></a>
                </li>
              <?php endif; ?>
            <li class="nav-item <?php echo is_active_page('kategori_artikel', 'active'); echo is_active_page('artikel', 'active'); echo is_active_page('slider', 'active'); ?>">
              <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-newspaper"></i>
                <p>
                  Artikel
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="<?php echo is_active_page('kategori_artikel', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('kategori_artikel/index'); ?>">
                    <span>Kategori Artikel</span>
                  </a>
                </li>
                <?php if (in_array("artikel.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('artikel', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('artikel/index'); ?>">
                      <span>Blog Artikel</span>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("slider.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('slider', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('slider/index'); ?>">
                      <span>Slider Highlight</span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item <?php echo is_active_page('agen', 'active'); echo is_active_page('kantor_cabang', 'active'); echo is_active_page('pegawai', 'active'); ?>">
              <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-th-list"></i>
                <p>
                  Data Pokok
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php if (in_array("agen.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('agen', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('agen/index') ?>">
                      <span>Agen</span>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("pegawai.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('pegawai', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('pegawai/index') ?>">
                      <span>Pegawai</span>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("kantor_cabang.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('kantor_cabang', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('kantor_cabang/index') ?>">
                      <span>Cabang</span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item <?php echo is_active_page('jamaah', 'active'); echo is_active_page('jamaah_birthday', 'active') ?>">
              <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-users"></i>
                <p>
                  Jama'ah
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php if (in_array("jamaah.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('jamaah', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('jamaah/index') ?>">
                      <span>Data Jamaah</span>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("jamaah_birthday.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('jamaah_birthday', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('jamaah_birthday/index') ?>">
                      <span>Ulang tahun Jamaah</span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item d-none <?php echo is_active_page('tabrur', 'active'); echo is_active_page('tabah', 'active') ?>">
              <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-gem"></i>
                <p>
                  Tabungan
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="<?php echo is_active_page('tabrur', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('tabrur/index') ?>">
                    <span>Tabrur</span>
                  </a>
                </li>
                <?php if (in_array("tabah.access", $userMenus)) : ?>
                  <li class="<?php echo is_active_page('tabah', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('tabah/index') ?>">
                      <span>Tabah</span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item <?php echo is_active_page('rekap', 'active') ?>">
              <a href="#" class="nav-link has-dropdown">
                <i class="fa fa-calendar"></i>
                <p>
                  Rekap
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="<?php echo (strpos(uri_string(), 'kota') !== false ? 'active' : '') ?>">
                  <a class="nav-link" href="<?php echo site_url('rekap/index/kota') ?>">
                    <span>Berdasarkan Kota</span>
                  </a>
                </li>
                <?php if (in_array("rekap.access", $userMenus)) : ?>
                  <li class="<?php echo (strpos(uri_string(), 'marketing') !== false ? 'active' : '') ?>">
                    <a class="nav-link" href="<?php echo site_url('rekap/index/marketing') ?>">
                      <span>Berdasarkan Marketing</span>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("rekap.access", $userMenus)) : ?>
                  <li class="<?php echo (strpos(uri_string(), 'seat') !== false ? 'active' : '') ?>">
                    <a class="nav-link" href="<?php echo site_url('rekap/index/seat') ?>">
                      <span>Berdasarkan Seat</span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>

            <li><a class="nav-link" href="<?php echo site_url('index/logout'); ?>"><i class="fas fa-sign-out-alt"></i>
                <span>Log Out</span></a>
            </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        {CONTENT}
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
          <!-- Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a> -->
        <div class="float-right d-none d-sm-inline-block">
        v2.3.0
        </div>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
    </div>
    <!-- ./wrapper -->
  </body>

</html>
