<?php $this->load->view('template/template_scripts') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header row p-3 align-items-center justify-content-between">
      <h3><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h3>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">
          <a href="<?php echo base_url('admin/Index'); ?>">Dashboard</a> / <?php echo isset($condition) ? $condition : ''; ?>
        </div>
      </div>
    </div>
  </section>
    <div class="section-body">
      <div class="card">
        <div class="card-body">
            <?php echo $output->output; ?>
        </div>
      </div>
  </div>
</div>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}

?>