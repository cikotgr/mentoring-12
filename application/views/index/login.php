<style type="text/css">
  .display-5 {
    color: #FFAE01;
  }
</style>
<div id="spinner-front">
  <img src="<?php echo base_url('public/ajax-loader.gif'); ?>" /><br>
  Loading...
</div>
<div id="spinner-back"></div>
<div id="app">
  <section class="section">
    <div class="d-flex flex-wrap align-items-stretch">
      <div class="col-lg-8 col-12 order-lg-1 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom" data-background="<?php echo base_url() ?>public/img/unsplash/login-bg-2.jpg">
        <div class="absolute-bottom-left index-2">
          <div class="text-light p-5 pb-2">
            <div class="mb-5 pb-3">
              <h3 class="mb-2 display-5 font-weight-bold"><?php echo $this->config->item('webname') ?></h3>
              <h5 class="font-weight-normal text-muted-transparent">Profesional, Kekeluargaan, Kebersamaan</h5>
            </div>
            <!-- Photo by <a class="text-light bb" target="_blank" href="https://www.instagram.com/kngmhfd/">Kang Mahfud</a> on <a class="text-light bb" target="_blank" href="https://www.instagram.com/">Instagram</a> -->
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-12 order-lg-2 min-vh-100 order-2 bg-white">
        <div class="p-4 m-3">
          <img src="<?php echo base_url() ?>public/img/logo.png" alt="logo" width="50%" class="mb-5 mt-2">
          <hr>
          <!-- <h4 class="text-dark font-weight-normal">Welcome to <span class="font-weight-bold"><a href="<?php echo site_url() ?>"><?php echo $this->config->item('webname') ?></a></span></h4> -->
          <div id="not_login"></div>
          <?php if (isset($successMessage)) : ?>
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-success alert-has-icon">
                  <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                  <div class="alert-body">
                    <div class="alert-title">Proses Berhasil !</div>
                    <p><?php echo $successMessage ?></p>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
          <?php if (isset($errorMessage)) : ?>
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-danger alert-has-icon">
                  <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                  <div class="alert-body">
                    <div class="alert-title">Mohon Maaf !</div>
                    <p><?php echo $errorMessage ?></p>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
          <?php echo form_open('/', array('id' => 'login-form')); ?>
          <div class="form-group">
            <label for="Username">Username/Email</label>
            <input id="Username" type="text" class="form-control" name="username" tabindex="1" required autofocus>
            <div class="invalid-feedback">
              Masukkan username anda
            </div>
          </div>

          <div class="form-group">
            <div class="d-block">
              <label for="password" class="control-label">Kata Sandi</label>
            </div>
            <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
            <div class="invalid-feedback">
              Masukkan kata sandi anda
            </div>
          </div>
          <hr>
          <div class="form-group text-right">
            <a href="auth-forgot-password.html" class="float-left mt-3 text-dark">
              Lupa kata sandi ?
            </a>
            <button type="submit" id="buttondefault" class="btn btn-success" name="login-button" value="Login Button"><i class="fas fa-sign-in-alt"></i> Masuk</button>
          </div>

          <div class="mt-5 text-center">
            <!-- Don't have an account? <a href="<?php echo site_url('front/register') ?>">Create new one</a> -->
          </div>
          </form>

          <div class="text-center mt-5 text-small">
            Copyright &copy; <?php echo $this->config->item('webname') ?>
            <!-- . Made with 💙 by Stisla -->
            <div class="mt-2">
              <a href="#">Kebijakan privasi</a>
              <div class="bullet"></div>
              <a href="#">Layanan</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>