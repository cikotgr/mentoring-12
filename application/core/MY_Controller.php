<?php
defined('BASEPATH') or exit('No direct script access allowed');


class BaseController extends CI_Controller {

    protected $template = "app";
    protected $module = "";
    protected $data = array();
    private $whitelistUrl = [
        '',
        'forgot_password',
        'forgot_password_reset',
        'aktifkan_akun',
        'login',
        'logout',    ];
    public $loginBehavior = true;

    public function __construct() {
        parent::__construct();
        $this->load->model('m_activity_log');
        $this->load->library('tanggalindo');
        $userId = $this->session->userdata('admin_userId');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('username', 'Username', ['required']);
        $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
        $this->form_validation->set_rules('password', 'Password', ['required']);
        $this->form_validation->set_message('required', '{field} tidak boleh kosong.');

        if (uri_string() == "" && $this->input->post("login-button") != null) { // login form submit
            $username = $this->input->post("username");
            $password = trim($this->input->post("password"));

            $result = $this->db->query("SELECT a.id_user, a.username, a.email, a.nama_lengkap,"
                    . " a.id_group, a.is_active, b.nama_group, b.keterangan, b.dbusername "
                    . " FROM user a INNER JOIN user_group b ON a.id_group = b.id_group "
                    . " WHERE (a.username = ? OR a.email = ?) "
                    . " AND a.password = SHA2(?, 256) "
                    . " AND a.is_active = 1", array($username, $username, $password));

            if ($result->num_rows() == 0) {
                $this->data["errorMessage"] = "Gagal login, silahkan periksa kembali informasi login Anda.";
                $this->data["user_temp"] = $username;
                $this->data["pass_temp"] = $password;
                $this->template = "login";
                $this->render("login");
            } else {
                $row = $result->first_row();
                $id_user = $row->id_user;
                $user = $row->username;
                $email = $row->email;
                $nama_lengkap = $row->nama_lengkap;
                $id_group = $row->id_group;
                $is_active = $row->is_active;
                $nama_group = $row->nama_group;
                $keterangan = $row->keterangan;
                $dbusername = $row->dbusername;


                $this->session->set_userdata("admin_userId", $id_user);
                $this->session->set_userdata("admin_username", $user);
                $this->session->set_userdata("admin_email", $email);
                $this->session->set_userdata("admin_nama", $nama_lengkap);
                $this->session->set_userdata("admin_idGroup", $id_group);
                $this->session->set_userdata("admin_isActive", $is_active);
                $this->session->set_userdata("admin_groupName", $nama_group);
                $this->session->set_userdata("admin_groupDescription", $keterangan);
                $this->session->set_userdata("admin_dbUsername", $dbusername);
                $this->config->set_item('database_name', $this->session->userdata('admin_dbUsername'));

                $this->setDatabase(); //CHANGE DATABASE BASED ON USER

                $this->setUserData();
                $this->setLog($id_user);
            }
        } else if (!$userId && uri_string() == "") { // Accessing index page and there is no user session (login form state)
            
            $this->template = "login";
            if ($this->input->get("access_without_login") == "true") {
                $this->data["errorMessage"] = "Session anda telah berakhir, silahkan login kembali untuk masuk ke dashboard.";
            } else if ($this->input->get("logout") == "true") {
                $this->data["successMessage"] = "Anda telah keluar.";
            } else if ($this->input->get("forgot_password") == "true") {
                $this->data["successMessage"] = "Password anda berhasil diperbarui. Silahkan login dengan password baru.";
            }
            $this->render("login");

        } else if (!$userId && !in_array(uri_string(), $this->whitelistUrl) && $this->loginBehavior) { // Accessing user page and there is no user session
            $this->template = "login";
            redirect("?access_without_login=true");
        } else if ($userId != null) { // Accessing user page and there is user session
            $this->setUserData();
        }


        if (is_array($this->input->get())) {
            foreach ($this->input->get() as $key => $value) {
                $this->data[$key] = $value;
            }
        }

        if (is_array($this->input->post())) {
            foreach ($this->input->post() as $key => $value) {
                if ($key == "description" || $key == "email" || $key == "is_active" || $key == "is_soft_delete" || $key == "username" || $key == "nama_lengkap") {
                    $this->data[$key . "Input"] = $value;
                } else {
                    $this->data[$key] = $value;
                }
            }
        }
    }

    protected function setUserData() {
        $this->data["admin_userId"] = $this->session->userdata("admin_userId");
        $this->data["admin_username"] = $this->session->userdata("admin_username");
        $this->data["admin_email"] = $this->session->userdata("admin_email");
        $this->data["admin_nama"] = $this->session->userdata("admin_nama");
        $this->data["admin_idGroup"] = $this->session->userdata("admin_idGroup");
        $this->data["admin_isActive"] = $this->session->userdata("admin_isActive");
        $this->data["admin_groupName"] = $this->session->userdata("admin_groupName");
        $this->data["admin_groupDescription"] = $this->session->userdata("admin_groupDescription");
        $this->data["admin_dbUsername"] = $this->session->userdata("admin_dbUsername");


        $result = $this->db->query("SELECT DISTINCT nama_modul, hak_akses FROM akses_group_modul WHERE id_group = ? ORDER BY nama_modul", array($this->session->userdata("admin_idGroup")));

        $this->data["userMenus"] = array();
        if ($result) {
            foreach ($result->result() as $row) {
                $this->data["userMenus"][] = $row->nama_modul . "." . $row->hak_akses;
            }
        }
    }

    protected function unSetUserData() {
        $this->session->unset_userdata("admin_userId");
        $this->session->unset_userdata("admin_username");
        $this->session->unset_userdata("admin_email");
        $this->session->unset_userdata("admin_nama");
        $this->session->unset_userdata("admin_idGroup");
        $this->session->unset_userdata("admin_isActive");
        $this->session->unset_userdata("admin_groupName");
        $this->session->unset_userdata("admin_groupDescription");
        $this->session->unset_userdata("admin_dbUsername");

    }

    protected function render($filename = null) {

        if (empty($this->session->userdata("admin_userId"))) {
            $this->template = "app_front";
        }

        $template = $this->load->view("template/" . $this->template, $this->data, true);

        $content = $this->load->view(($this->module != "" ? $this->module . "/" : "") . strtolower(get_class($this)) . "/" . $filename, $this->data, true);

        if ($this->module != NULL) {
            if (in_array($this->module . ".access", $this->data["userMenus"]) == 0) {
                $message = "Maaf, Anda tidak memiliki akses ke halaman ini.";
                echo "<script type='text/javascript'>alert('$message');</script>";
                redirect();
            }
        }
        exit(str_replace("{CONTENT}", $content, $template));
    }

    protected function cek_hak_akses($hak_akses) {
        $cek = $this->db->query("SELECT * FROM `akses_group_modul` WHERE nama_modul=? AND hak_akses=? AND id_group=?", array($this->module, $hak_akses, $this->session->userdata("admin_idGroup")))->row_array();
        if (empty($cek)) {
            $message = "Maaf, Anda tidak memiliki akses ke halaman ini.";
            echo "<script type='text/javascript'>alert('$message');</script>";
            redirect();
        } else {
            $hak_akses = $this->db->query("SELECT hak_akses FROM `akses_group_modul` WHERE nama_modul=? AND id_group=?", array($this->module, $this->session->userdata("admin_idGroup")))->result_array();
            foreach ($hak_akses as $row) {
                $hasil[] = $row['hak_akses'];
            }
            return $hasil;
        }
    }

    protected function setDatabase() {
        $dbUsername = $this->session->userdata('admin_dbUsername');

        $this->load->database($dbUsername, FALSE, TRUE); //CHANGE DATABASE BASED ON USER
    }

    protected function setLog($id_user) {
        $activity = "LOG IN";
        $page_url = base_url();

        $this->m_activity_log->insert($id_user, $activity, $page_url);
    }

    protected function checkAccess($module, $requiredAccess)
    {
        $access = $this->db->query("SELECT hak_akses FROM akses_group_modul WHERE nama_modul = ? AND id_group = ?", array($module, $this->session->userdata("admin_idGroup")))->result_array();

        $userAccess = array();

        foreach ($access as $row) {
            $userAccess[] = $row['hak_akses'];
        }

        $hasAccess = false;

        foreach ($requiredAccess as $permission) {
            if (in_array($permission, $userAccess)) {
                $hasAccess = true;
                break;
            }
        }

        return $hasAccess;
    }
}
