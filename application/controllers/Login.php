<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends BaseController {

    function index() {
        $this->template = "login";
        $template = $this->load->view("template/" . $this->template, $this->data, true);
        $content = $this->load->view('index/login', $this->data, true);
        exit(str_replace("{CONTENT}", $content, $template));
    }

    function masuk($group) {
        $cek_modul = $this->db->query("SELECT id_group FROM akses_group_modul WHERE id_group = ?", $group);
        if ($cek_modul->num_rows() == 0) {

            $this->session->unset_userdata("t_userId");
            $this->session->unset_userdata("t_username");
            $this->session->unset_userdata("t_realName");
            $this->session->unset_userdata("t_email");
            $this->session->unset_userdata("t_creationTime");
            $this->session->unset_userdata("t_isActive");

            $this->data["errorMessage"] = "Maaf modul untuk group tersebut belum tersedia.
                                        Silahkan login menggunakan role lain.";

            $this->template = "login";
            $template = $this->load->view("template/" . $this->template, $this->data, true);
            $content = $this->load->view('index/login', $this->data, true);
            exit(str_replace("{CONTENT}", $content, $template));
        } else {
            $userId = $this->session->userdata('t_userId');
            $groups = $this->db->query("SELECT ug.id_group, ug.nama_group
                                        FROM user_group ug
                                        WHERE ug.id_group = ?", array($group))->first_row();

            $instansi = $this->db->query("SELECT kode_kecamatan as kode_intansi, nama_kecamatan as nama_instansi FROM kecamatan where id_user = ?"
                                . "UNION SELECT kode_kabupaten as kode_intansi, nama_kabupaten as nama_instansi FROM kabupaten where id_user = ? ", array($userId, $userId))->first_row();
            if (!empty($pegawai)) {
                $this->session->set_userdata("kode_intansi", $instansi->kode_intansi);
                $this->session->set_userdata("nama_instansi", $instansi->nama_instansi);
            }
            $this->session->set_userdata("t_idGroup", $groups->id_group);
            $this->session->set_userdata("t_groupName", $groups->nama_group);
            redirect('index');
        }
    }

    public function forgot()
    {
        $this->template = "login";
        $template = $this->load->view("template/" . $this->template, $this->data, true);
        $content = $this->load->view('index/modal_group', $this->data, true);
        exit(str_replace("{CONTENT}", $content, $template));
    }

}
