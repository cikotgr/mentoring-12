<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "akun";

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Akun Akuntansi';
        $this->data['condition'] = '<div class="breadcrumb-item active">Data Akun</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('akun');


        // $crud->where(['mata_kuliah.is_active' => '1']);
        $crud->columns(['nama_akun']);

        $crud->fields(['nama_akun']);
        $crud->requiredFields(['nama_akun']);
        $crud->defaultOrdering('nama_akun', 'asc');

        $crud->displayAs('nama_akun', 'Nama Akun');

        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output);
    }

    function _setOutput($output = null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('akun/index/index', $x);
        $this->layout->publish();
    }
}
