<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "akun";

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        if (!$this->checkAccess('lembaga', ['access'])) {
            redirect('/');
        }
        $this->data['nama_menu'] = 'Data Akun Akuntansi';
        $this->data['condition'] = '<div class="breadcrumb-item active">Data Akun</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('akun');


        // $crud->where(['mata_kuliah.is_active' => '1']);
        $crud->columns(['nama_akun']);

        $crud->fields(['nama_akun']);
        $crud->requiredFields(['nama_akun']);
        $crud->defaultOrdering('nama_akun', 'asc');

        $crud->displayAs('nama_akun', 'Nama Akun');

        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output);
    }

    function _setOutput($output = null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('lembaga/index/index', $x);
        $this->layout->publish();
    }

    public function DataTablehighlight()
    {
        $return = array();

        $field = array(
            'sSearch',
            'iSortCol_0',
            'sSortDir_0',
            'iDisplayStart',
            'iDisplayLength',
        );

        foreach ($field as $v) {
            $$v = $this->input->get_post($v);
        }

        $return = array(
            "sEcho" => $this->input->post('sEcho'),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array()
        );

        $params = array(
            'sSearch' => $sSearch,
            'start' => $iDisplayStart,
            'limit' => $iDisplayLength,
        );
        $btn = '';

        $data = $this->M_highlight->DataTablehighlight($params);
        if ($data['total'] > 0) {
            $return['iTotalRecords'] = $data['total'];
            $return['iTotalDisplayRecords'] = $return['iTotalRecords'];

            foreach ($data['rows'] as $k => $row) {

                if ($row['is_active'] == '1') {
                    $btn = '<button class="btn btn-secondary text-dark btn-sm" type="button" id="btn-lock-highlight" data="' . $row['id_highlight'] . '"><i class="fas fa-unlock"></i></button>&nbsp;';
                } else if ($row['is_active'] == '0') {
                    $btn = '<button class="btn btn-light btn-sm" type="button" id="btn-unlock-highlight" data="' . $row['id_highlight'] . '"><i class="fas fa-lock"></i></button>&nbsp;';
                }

                $row['nomor'] = '<p class="text-center">' . ($iDisplayStart + ($k + 1)) . '</p>';
                $row['nama_highlight'] = $row['nama_highlight'];
                $row['is_active'] = ($row['is_active'] == '1' ? '<span class="badge badge-success"><i class="fas fa-check"></i> Aktif</span>' : '<span class="badge badge-warning"><i class="fas fa-ban"></i> Tidak AKtif</span>');
                $row['kelola'] = $btn . '<button class="btn btn-warning btn-sm" type="button" id="btn-edit-highlight" data="' . $row['id_highlight'] . '"><i class="fas fa-edit"></i></button>&nbsp;<button class="btn btn-danger btn-sm" type="button" id="btn-hapus-highlight" data="' . $row['id_highlight'] . '"><i class="fas fa-trash"></i></button>';

                $return['aaData'][] = $row;
            }
        }
        $this->db->flush_cache();
        echo json_encode($return);
    }

    public function simpan()
    {
        $response['success'] = false;
        $response['pesan'] = '';

        $input = $this->input->post(null, true);

        $highlight['nama_highlight'] = $input['nama_highlight'];
        $highlight['created_by'] = $this->session->userdata('t_userId');
        $highlight['is_active'] = '1';

        $result = $this->M_highlight->insert($highlight);
        if ($result != true) {
            $response['success'] = false;
            $response['pesan'] = 'Entri data gagal';
        } else {
            $response['success'] = true;
            $response['pesan'] = 'Data highlight berhasil ditambahkan';
        }
        echo json_encode($response);
    }

    public function edit()
    {
        $result = $this->M_highlight->get($this->input->post('kode'), TRUE);
        echo json_encode($result);
    }

    public function update()
    {
        $response['success'] = false;
        $response['pesan'] = '';

        $input = $this->input->post(null, true);

        $highlight['nama_highlight'] = $input['nama_highlight'];

        $result = $this->M_highlight->update($highlight, array('id_highlight' => $input['id_highlight']));

        if ($result === false) {
            $response['success'] = false;
            $response['pesan'] = 'Update data gagal';
        } else {
            $response['success'] = true;
            $response['pesan'] = 'Data highlight berhasil diupdate';
        }


        echo json_encode($response);
    }


    public function delete()
    {
        $response['success'] = false;
        $response['pesan'] = '';

        $result = $this->M_highlight->delete($this->input->post('kode'));


        if ($result === false) {
            $response['success'] = false;
            $response['pesan'] = 'Proses hapus data gagal';
        } else {
            $response['success'] = true;
            $response['pesan'] = 'Data highlight berhasil dihapus';
        }
        echo json_encode($response);
    }

    public function lockHighlight()
    {
        $response['success'] = false;
        $response['pesan'] = '';
        $response['type'] = '';

        $params = $this->input->post(null, true);

        if ($params['status'] == 'kunci') {
            $data['is_active'] = '0';
            $response['type'] = 'kunci';
            $response['pesan'] = 'Data highlight berhasil dikunci';
        } else if ($params['status'] == 'buka') {
            $data['is_active'] = '1';
            $response['type'] = 'buka';
            $response['pesan'] = 'Data highlight berhasil dibuka';
        }

        $result = $this->M_highlight->update($data, array('id_highlight' => $params['kode']));

        if ($result === false) {
            $response['success'] = false;
            $response['pesan'] = 'Update data gagal';
        } else {
            $response['success'] = true;
        }

        echo json_encode($response);
    }
}
