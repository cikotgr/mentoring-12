<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Tabah extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_tabah', 'tabah');
    }


    public function histori_post()
    {

        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents('php://input')), true);

                if ($data['fk_id_user']!='' || $data['fk_id_user']!=NULL) {

                    $result = $this->tabah->getHistoriCustomer($data);

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data parameter tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function setoranAwal_post()
    {
        $user = $this->input->post('id_user');
        $url_bukti = '';

        if ($user != '' || $user !=NULL) {

            if (isset($_FILES['url_bukti']['name'])) {
                list($width, $height) = getimagesize($_FILES['url_bukti']['tmp_name']);
                $config['upload_path'] = 'files/tabah/setoran_awal/'; //path folder file upload
                $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                $config['max_size'] = '2000';
                $config['file_name'] = "setoran_awal_" . date('ymdhis'); //enkripsi file name upload
                $this->load->library('upload');
                $this->upload->initialize($config);
                if ($this->upload->do_upload('url_bukti')) {
                    $file_foto = $this->upload->data();
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './files/tabah/setoran_awal/' . $file_foto['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['quality'] = '50%';
                    $config['width'] = round($width / 2);
                    $config['height'] = round($height / 2);
                    $config['new_image'] = './files/tabah/setoran_awal/' . $file_foto['file_name'];
                    $this->load->library('image_lib');
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                    $nama_foto = 'files/tabah/setoran_awal/' . $file_foto['file_name'];
                    $url_bukti = $nama_foto;
                }
            }

            $data['id_user'] = $this->input->post('id_user');
            $data['id_tabura'] = $this->input->post('id_tabura');
            $data['nominal'] = $this->input->post('nominal');
            $data['mekanisme'] = $this->input->post('mekanisme');
            $data['catatan'] = $this->input->post('catatan');
            $data['tanggal_pembayaran'] = $this->input->post('tanggal_pembayaran');
            $data['url_bukti'] = $url_bukti;

            $result = $this->tabah->setoranAwal($data);

            if ($result['status']!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }


        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter kosong, harap lengkapi data anda',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function pembayaran_post()
    {
        $url_bukti = '';
        if (isset($_FILES['url_bukti']['name'])) {
            list($width, $height) = getimagesize($_FILES['url_bukti']['tmp_name']);
            $config['upload_path'] = 'files/pembayaran/'; //path folder file upload
            $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
            $config['max_size'] = '2000';
            $config['file_name'] = "konfirmasi_pembayaran_" . date('ymdhis'); //enkripsi file name upload
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('url_bukti')) {
                $file_foto = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = './files/pembayaran/' . $file_foto['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '50%';
                $config['width'] = round($width / 2);
                $config['height'] = round($height / 2);
                $config['new_image'] = './files/pembayaran/' . $file_foto['file_name'];
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $nama_foto = 'files/pembayaran/' . $file_foto['file_name'];
                $url_bukti = $nama_foto;
            }
        }

        $data['url_bukti'] = $url_bukti;
        $data['nomor_invoice'] = $this->input->post('nomor_invoice');
        $data['jenis_pembayaran'] = $this->input->post('jenis_pembayaran');
        $data['tanggal_transfer'] = $this->input->post('tanggal_transfer');
        $data['bank_tujuan'] = $this->input->post('bank_tujuan');
        $data['jumlah_dana'] = $this->input->post('jumlah_dana');
        $data['nama_pengirim'] = $this->input->post('nama_pengirim');
        $data['email_jamaah'] = $this->input->post('email_jamaah');
        $data['no_telpon_jamaah'] = $this->input->post('no_telpon_jamaah');
        $data['status'] = '01';

        $result = $this->tabah->konfirmasiPembayaran($data);

        if ($result['status']!='failed') {

            $this->response([
                'status'=>$this->ok,
                'message'=>$result['message'],
                'data'=>$result['data']], REST_Controller::HTTP_OK);

        }else {
            $this->response([
            'status'=>$this->bad,
            'message'=>$result['message'],
            'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function cekPembayaran_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);

        if ($data['kode_pembayaran']!='' || $data['kode_pembayaran']!=NULL) {

            $result = $this->tabah->cekPembayaran($data);

            if ($result!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Parameter tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }




}
