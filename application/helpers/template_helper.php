<?php

function get_template_directory($path, $dir_file)
{
	global $SConfig;

	$replace_path = str_replace('\\', '/', $path);
	$get_digit_doc_root = strlen($SConfig->_document_root);

	$full_path = substr($replace_path, $get_digit_doc_root);
	return $SConfig->_site_url.$full_path.'/'.$dir_file;
}

function get_template($view)
{
	$_this =& get_instance();
	return $_this->site->view($view);
}

function set_url($sub)
{
	$_this =& get_instance();
	if ($_this->site->side == 'backend' && $_this->uri->segment(1)=='admin') {
		return site_url('admin/'. $sub);
	}else if($_this->site->side == 'backend' && $_this->uri->segment(1)=='members'){
		return site_url('members/'. $sub);
	}else if($_this->site->side == 'backend'){
		return site_url( $sub);
	}
}

function is_active_page($page, $class)
{
	$_this =& get_instance();
	if ($page == $_this->uri->segment(1) || $page == $_this->uri->segment(2)) {
		return $class;
	}
}

function title()
{
	$_this =& get_instance();

	global $SConfig;

	$array_backend_page = array(
		'dashboard' => 'Dashboard',
		'member' => 'Data Member',
		'pengelola' =>'Data Pengelola Aplikasi',
		'hasil' =>'Hasil Pengujian',
		''=>'Login',
		'login' => 'Login',
		'register' => 'Registrasi Member baru',
		'pertanyaan' =>'Tes bersama'
	);

	$title = NULL;
	if ($_this->site->side =='backend' && (array_key_exists($_this->uri->segment(2), $array_backend_page))) {
		return $array_backend_page[$_this->uri->segment(2)].' | '.$SConfig->_cms_name;;
	}
}

function getUUID()
{
	$ci =& get_instance();
	$result = $ci->db->query("SELECT UUID()")->row_array()['UUID()'];
	return $result;
}

function getAuthor($id)
{
	$ci =& get_instance();
	$result = $ci->db->query("SELECT username from user where id_user=?", $id)->row_array()['username'];
	return $result;
}

function setForm($class_group="",$type="", $name="",$label="", $placeholder="",$required="",$value="")
{
	$tag_form = '';
	$form_type ='';

	if ($type=='text' || $type=='password'||$type=='number'|| $type=='email'||$type=='file') {
		$form_type .= '<input type="'.$type.'" class="form-control" name="'.$name.'" id="'.$name.'" value="'.$value.'" placeholder="'.$placeholder.'" '.$required.'>';
	}else if($type=='textarea'){
		$form_type .= '<textarea class="form-control" name="'.$name.'" id="'.$name.'" placeholder="'.$placeholder.'" '.$required.' style="height:150px;">'.$value.'</textarea>';
	}

	if ($class_group=='horizontal') {
		$tag_form = '<div class="form-group row">
	                      <label for="'.$name.'" class="col-sm-3 col-form-label">'.$label.'</label>
	                      <div class="col-sm-9">
	                        '.$form_type.'
	                      </div>
	                    </div>';
	} else if($class_group=='vertical'){
		$tag_form = '<div class="form-group">
                      <label>'.$label.'</label>
                      '.$form_type.'
                    </div>';
	}

    return $tag_form;
}

if (!function_exists('auto_code')) {
	function auto_code($prefix, $delim="", $position="append")
	{
		$ci  =& get_instance();
        $values = array( $prefix );
        $ci->db->query("INSERT INTO auto_code (prefix, sequence ) VALUES ( ?, 1 ) ON DUPLICATE KEY UPDATE sequence  =  sequence + 1", $values );
        $result  =  $ci->db->query("SELECT sequence FROM auto_code WHERE prefix = ?", $values );
        $row  =  $result->row();
        if( $position == "append" ) {
            $result  =  strtoupper($prefix) . $delim . str_pad($row->sequence, 5, '0', STR_PAD_LEFT);
        }
        else {
            $result  =  str_pad($row->sequence, 5, '0', STR_PAD_LEFT). $delim . strtoupper($prefix);
        }

        return $result;
	}
}
	function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function cekUserExist($username)
    {
    	$ci =& get_instance();

        $ci->db->start_cache();
        $ci->db->select('*');
        $ci->db->from('user');
        $ci->db->group_start();
        $ci->db->where('username',$username);
        // $this->db->or_where('email',$username);
        $ci->db->group_end();
        $ci->db->stop_cache();
        $data['data']=$ci->db->get()->row_array();
        $data['jumlah']=$ci->db->count_all_results();
        $ci->db->flush_cache();
        return $data;
    }

    function sendUcapanUltah($params)
	{
	    $userkey = '889abc8a526c';
	    $passkey = 'bb2b20ef51e03cd7a6686f0b';
	    $message = '*PT. Rosana & Rosana Group mengucapkan.*
	Selamat ulang tahun yang ke '.$params['umur'].' untuk '.$params['identitas'].'. Semoga Allah SWT senantiasa memberikan Rahmat, Taufiq, Hidayah dan Inayah-Nya. Amin
	_Wassalam_
	Pimpinan & Segenap Karyawan
	*PT. Rosana & Rosana Group*';
	    $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
	    $curlHandle = curl_init();
	    curl_setopt($curlHandle, CURLOPT_URL, $url);
	    curl_setopt($curlHandle, CURLOPT_HEADER, 0);
	    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
	    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
	    curl_setopt($curlHandle, CURLOPT_POST, 1);
	    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
	        'userkey' => $userkey,
	        'passkey' => $passkey,
	        'to' => $params['nomor_hp'],
	        'message' => $message
	    ));
	    $results = json_decode(curl_exec($curlHandle), true);
	    curl_close($curlHandle);
	    return $results;
	}

	function send_pin($params)
{
    $userkey = '889abc8a526c';
    $passkey = 'bb2b20ef51e03cd7a6686f0b';
    $message = '*Rosana Tour and Travel*
Untuk melanjutkan proses pendaftaran, silahkan masukkan PIN berikut pada halaman pendaftaran.
'.$params['pin_pendaftaran'].'
*Terima Kasih*';
    $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
    $curlHandle = curl_init();
    curl_setopt($curlHandle, CURLOPT_URL, $url);
    curl_setopt($curlHandle, CURLOPT_HEADER, 0);
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
    curl_setopt($curlHandle, CURLOPT_POST, 1);
    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
        'userkey' => $userkey,
        'passkey' => $passkey,
        'to' => $params['nomor_hp'],
        'message' => $message
    ));
    $results = json_decode(curl_exec($curlHandle), true);
    curl_close($curlHandle);
    return $results;
}

function send_pin_forgot($params)
{
    $userkey = '889abc8a526c';
    $passkey = 'bb2b20ef51e03cd7a6686f0b';
    $message = '*Rosana Tour and Travel*
Masukan PIN pendaftaran berikut ini untuk melakukan konfirmasi perubahan kata sandi.
'.$params['pin_pendaftaran'].'
*Terima Kasih*';
    $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
    $curlHandle = curl_init();
    curl_setopt($curlHandle, CURLOPT_URL, $url);
    curl_setopt($curlHandle, CURLOPT_HEADER, 0);
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
    curl_setopt($curlHandle, CURLOPT_POST, 1);
    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
        'userkey' => $userkey,
        'passkey' => $passkey,
        'to' => $params['nomor_hp'],
        'message' => $message
    ));
    $results = json_decode(curl_exec($curlHandle), true);
    curl_close($curlHandle);
    return $results;
}

function KirimEmail($params)
{

	require APPPATH.'libraries/phpmailer/src/Exception.php';
    require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
    require APPPATH.'libraries/phpmailer/src/SMTP.php';
    date_default_timezone_set('Asia/Jakarta');

	$response['status'] = true;
    $response['info'] = '';

	$mail = new PHPMailer();

    $mail->IsHTML(true);    // set email format to HTML
    $mail->IsSMTP();   // we are going to use SMTP
    $mail->SMTPAuth   = true; // enabled SMTP authentication
    $mail->SMTPSecure = "ssl"; // prefix for secure protocol to connect to the server
    // $mail->SMTPDebug  = 2;
    $mail->Host       = "smtp.gmail.com"; // setting GMail as our SMTP server
    $mail->Port       = 465;   // SMTP port to connect to GMail
    $mail->Username   = "cyber.rosana@gmail.com";  // alamat email kamu
    $mail->Password   = "www.rosanatourtravel.com";            // password GMail
    $mail->SetFrom("cyber.rosana@gmail.com", 'Penting !');  //Siapa yg mengirim email
    $mail->Subject    = 'Invoice pesanan anda';
    $mail->Body       = $params['view'];
    $mail->AddAddress($params['data']['email']);

    if($mail->send())
        {
            $response['status'] = true;
            $response['info'] = '';
        }
        else
        {
            $response['status'] = false;
            $response['info'] = $mail->ErrorInfo;
        }
        return $response;
}

 ?>